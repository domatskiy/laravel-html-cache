<?php

namespace Domatskiy\Tests;

use Carbon\Carbon;

use Domatskiy\HtmlCache\CacheKeysRegistrator;
use Domatskiy\HtmlCache\HtmlStorage;
use Domatskiy\HtmlCache\HtmlCacheServiceProvider;

use Domatskiy\HtmlCache\Middleware\HtmlCacheMiddleware;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Orchestra\Testbench\TestCase as TestCase;
use Symfony\Component\HttpFoundation\HeaderBag;

class HtmlCacheTest extends TestCase
{
    /**
     * @var $CacheKeysRegistrator CacheKeysRegistrator
     */
    protected $CacheKeysRegistrator;

    protected function getPackageProviders($app)
    {
        return [HtmlCacheServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
            'HtmlStorage' => HtmlStorage::class,
            'DBHtmlPath' => HtmlStorage\HtmlPath::class,
            'DBHtmlPathKey' => HtmlStorage\HtmlPath\Key::class,
        ];
    }

    public function testRequest()
    {

        $middleware = new HtmlCacheMiddleware();
        $expectedStatusCode = 401;
        try {
            $content = 'zzzzzzzzzzzzzz';
            $request = Request::create(config('app.url'), 'GET',[],[],[],['REMOTE_ADDR'=>'127.0.0.1']);

            $response = $middleware->handle($request, function () use ($content) {
                $Response = new Response();
                $Response->setContent($content);
                return $Response;
            });
            $this->assertEquals(200, $response->getStatusCode());
            $this->assertEquals($content, $response->getContent());
            // var_dump($response->headers->get('Last-Modified'));

            $response = $middleware->handle($request, function () use ($content) {
                $Response = new Response();
                $Response->setContent($content);
                return $Response;
            });
            $this->assertEquals(200, $response->getStatusCode());
            $this->assertEquals($content, $response->getContent());
            //var_dump($response->headers->get('Last-Modified'));

            $request = Request::create(config('app.url'), 'GET', [],[],[],['REMOTE_ADDR'=>'127.0.0.1']);

            $HeaderBag = new HeaderBag();
            $HeaderBag->add([
                'If-Modified-Since' => $response->headers->get('Last-Modified')
            ]);

            $request->headers = $HeaderBag;

            $response = $middleware->handle($request, function () use ($content) {
                $Response = new Response();
                $Response->setContent($content);
                return $Response;
            });
            $this->assertEquals(304, $response->getStatusCode());
            $this->assertEquals('', $response->getContent());
        }catch(\Symfony\Component\HttpKernel\Exception\HttpException $e){
            $this->assertEquals(
                $expectedStatusCode,
                $e->getStatusCode(),
                sprintf("Expected an HTTP status of %d but got %d.", $expectedStatusCode, $e->getStatusCode())
            );
        } catch (FileNotFoundException $e) {
            $this->assertEquals(
                $expectedStatusCode,
                $e->getStatusCode(),
                sprintf("Expected an HTTP status of %d but got %d.", $expectedStatusCode, $e->getStatusCode())
            );
        } catch (\League\Flysystem\FileNotFoundException $e) {
            $this->assertEquals(
                $expectedStatusCode,
                $e->getStatusCode(),
                sprintf("Expected an HTTP status of %d but got %d.", $expectedStatusCode, $e->getStatusCode())
            );
        }
    }

    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('html-cache.driver', 'local');
        $app['config']->set('html-cache.path', 'html_cache');

        $app['config']->set('database.default', 'testing');

        $app['config']->set('database.connections.testing', [
            'driver'   => 'sqlite',
            'database' => ':memory:',
            'prefix'   => '',
        ]);
    }

    /**
     * Resolve application Console Kernel implementation.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function resolveApplicationConsoleKernel($app)
    {
        #$app->singleton('Illuminate\Contracts\Console\Kernel', 'Illuminate\Foundation\Console\Kernel');
        $app->singleton('Illuminate\Contracts\Console\Kernel', '\Orchestra\Testbench\Console\Kernel');
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../src/migrations');
        $this->loadLaravelMigrations(['--database' => 'testing']);

        /*$config = [
            'html-cache.driver' => 'local',
            'html-cache.path'   => 'html_cache',
        ];

        config($config);*/

        $this->CacheKeysRegistrator = $this->app->make('CacheKeysRegistrator');
    }

    public function testMisc()
    {
        // test object initialization
        $this->assertInstanceOf(CacheKeysRegistrator::class, $this->CacheKeysRegistrator);

        $key = 'test-key';
        $this->CacheKeysRegistrator->addKey($key);
        $keys = $this->CacheKeysRegistrator->getKeys();

        $this->assertTrue(is_array($keys));
        $this->assertTrue(in_array($key, $keys));
    }

    /** @test */
    public function it_runs_the_migrations()
    {
        $tested_key = 'test-key';
        $now = Carbon::now();

        $res = \DB::table('html_cache')->insert([
            'path' => 'test/path',
            'created_at' => $now,
            'updated_at' => $now,
        ]);

        $htmlCache = \DB::table('html_cache')
            ->where('id', '=', 1)
            ->first();

        $this->assertEquals('test/path', $htmlCache->path, 'path not found');

        if($htmlCache)
        {
            $now = Carbon::now();

            \DB::table('html_cache_key')->insert([
                'cache_id' => $htmlCache->id,
                'key' => $tested_key,
                'created_at' => $now,
                'updated_at' => $now,
            ]);

            $htmlCacheKey = \DB::table('html_cache_key')
                ->where('id', '=', 1)
                ->first();

            $this->assertEquals($tested_key, $htmlCacheKey->key, 'cache key not found');
        }

        HtmlStorage::deleteByKey($tested_key);

        $htmlCacheKey = \DB::table('html_cache_key')
            ->where('key', '=', $tested_key)
            ->first();

        # TODO check base and html files
        #$this->assertTrue(!$htmlCacheKey || $htmlCacheKey->count() === 0);
    }
}
