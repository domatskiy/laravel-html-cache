<?php

namespace Domatskiy\HtmlCache;

use Domatskiy\HtmlCache\HtmlStorage\Content;
use Domatskiy\HtmlCache\HtmlStorage\HtmlPath;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;

/**
 * Class HtmlStorage
 * @package Domatskiy\HtmlCache
 */
class HtmlStorage
{
    protected $disk = 'html_cache';

    protected $path;
    protected $file_name;
    protected $file_path;

    protected static $storage = null;

    /**
     * HtmlStorage constructor.
     * @param string $path
     * @param array $request_params
     */
    function __construct(string $path, array $request_params = [])
    {
        $this->path = $path;

        if (empty($request_params)) {
            $this->file_name = 'index';
        } else {
            $this->file_name = md5($path.serialize($request_params));
        }

        $this->file_path = $this->path.'/'.$this->file_name.'.html';
    }

    /**
     * @desc createLocalDriver
     * @return FilesystemAdapter
     * @throws Exception
     */
    public static function getStorage(): FilesystemAdapter
    {
        if (static::$storage instanceof FilesystemAdapter) {
            return static::$storage;
        }

        #$disk = config('html-cache.disk');
        $dir = config('html-cache.dir', 'html_cache');

        if (!$dir || !is_string($dir)) {
            throw new Exception('not correct dir to the config file');
        }

        static::$storage = \Storage::createLocalDriver([
            'root' => public_path($dir),
        ]);

        return static::$storage;
    }

    /**
     * @desc get content from html file
     * @return Content
     * @throws FileNotFoundException
     * @throws \League\Flysystem\FileNotFoundException|Exception
     */
    public function getContent(): Content
    {
        /**
         * проверка файла
         */

        $storage = self::getStorage();

        if ($storage->exists($this->file_path)) {
            /*
            array:4 [▼
              "type" => "file"
              "path" => "rent/apartment.html"
              "timestamp" => 1533115179
              "size" => 223859
            ]
            */

            $meta = $storage->getMetadata($this->file_path);
            $content = $storage->get($this->file_path);

            $date = new \DateTime();
            $date->setTimestamp($meta['timestamp']);

            return new Content($content, $date);
        }

        return new Content();
    }

    /**
     * @desc save content to html file
     * @param string $content
     * @return bool
     * @throws Exception
     */
    public function saveContent(string $content): bool
    {
        # Log::debug('saveContent');
        /**
         * @var $CacheKeysRegistrator CacheKeysRegistrator
         */
        $CacheKeysRegistrator = app()->make('CacheKeysRegistrator');
        $cache_keys = $CacheKeysRegistrator->getKeys();

        $storage = self::getStorage();

        if (!$storage->has($this->file_path) && $content) {
            $storage->put($this->file_path, $content);

            if (!empty($cache_keys)) {
                $cachePath = HtmlStorage\HtmlPath::firstOrCreate([
                    'path' => $this->file_path
                ]);

                if ($cachePath) {
                    foreach ($cache_keys as $key) {
                        HtmlStorage\HtmlPath\Key::firstOrCreate([
                            'cache_id' => $cachePath->id,
                            'key' => $key,
                        ]);
                    }
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @desc remove the html cache files directory
     * @throws Exception
     */
    function clear():void
    {
        $storage = self::getStorage();
        if ($storage->has($this->path)) {
            # remove dir
            $storage->deleteDir($this->path);
        }
    }

    /**
     * @desc to remove the html cache files
     * @throws Exception
     */
    public static function removeAllHtmlFiles()
    {
        $storage = self::getStorage();

        $dirs = $storage->directories();

        foreach ($dirs as $dir) {
            $storage->deleteDirectory($dir);
        }

        $files = $storage->files('/');

        foreach ($files as $file) {
            if ($file == '.hgignore' || $file == '.gitignore') {
                continue;
            }

            $storage->delete($file);
        }

    }

    /**
     * @desc delete the html cache for the cache key
     * @param $key
     * @throws Exception
     */
    public static function deleteByKey($key)
    {
        // Log::debug('deleteByKey: '.$key);

        $paths = HtmlPath::select([
            'html_cache.path as path',
            ])
            ->join('html_cache_key', 'html_cache_key.cache_id', '=', 'html_cache.id')
            ->where('html_cache_key.key', '=', $key)
            ->groupBy(['html_cache.path'])
            ->get();

        foreach ($paths as $path) {
            // Log::debug('deleteByKey: delete html cache '.$path->path);

            $storage = self::getStorage();

            if ($storage->exists($path->path)) {
                $storage->delete($path->path);
            } else {
                Log::warning('file not exits: '.$path->path);
            }

            HtmlPath::where('path', '=', $path->path)
                ->delete();
        }
    }

    /**
     * @desc clear tables
     */
    public static function truncateBase()
    {
        // Log::info('HtmlStorage::truncateBase');

        #\DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        Schema::disableForeignKeyConstraints();

        // очистка тегов
        \DB::table(HtmlPath::getModel()->getTable())
            ->truncate();

        // очистка ключей
        \DB::table(HtmlPath\Key::getModel()->getTable())
            ->truncate();

        #\DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        Schema::enableForeignKeyConstraints();
    }
}
