<?php

namespace Domatskiy\HtmlCache\Listeners;

use Domatskiy\HtmlCache\CacheKeysRegistrator;
use Domatskiy\HtmlCache\HtmlStorage;

use Illuminate\Cache\Events\KeyWritten;
use Illuminate\Cache\Events\KeyForgotten;
use Illuminate\Cache\Events\CacheHit;
use Illuminate\Support\Facades\Log;

/**
 * Class CacheEventSubscriber
 * @package Domatskiy\HtmlCache\Listeners
 */
class CacheEventSubscriber
{
    public function onKeyWritten(KeyWritten $event) {

        # Log::debug('CacheEventSubscriber::onKeyWritten key='.$event->key.', minutes='.$event->minutes);

        /**
         * @var $CacheKeysRegistrator CacheKeysRegistrator
         */
        $CacheKeysRegistrator = app()->make('CacheKeysRegistrator');
        $CacheKeysRegistrator->addKey($event->key);
    }

    public function CacheHit(CacheHit $event)
    {
        # Log::debug('CacheEventSubscriber::CacheHit key='.$event->key);
    }

    public function onKeyForgotten(KeyForgotten $event) {

        # Log::debug('CacheEventSubscriber::onKeyForgotten key='.$event->key);

        $CacheKeysRegistrator = app()->make('CacheKeysRegistrator');
        $CacheKeysRegistrator->deleteKey($event->key);

        // очистка html кэша по ключу
        HtmlStorage::deleteByKey($event->key);
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Cache\Events\KeyWritten',
            '\Domatskiy\HtmlCache\Listeners\CacheEventSubscriber@onKeyWritten'
        );

        /*$events->listen(
            'Illuminate\Cache\Events\CacheHit',
            '\Domatskiy\HtmlCache\Listeners\CacheEventSubscriber@CacheHit'
        );*/

        $events->listen(
            'Illuminate\Cache\Events\KeyForgotten',
            '\Domatskiy\HtmlCache\Listeners\CacheEventSubscriber@onKeyForgotten'
        );
    }
}
