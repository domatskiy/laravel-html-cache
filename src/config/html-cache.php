<?php

return [
    'disk' => 'public',
    'dir' => 'html_cache',
    'for_authorized' => true,
];
