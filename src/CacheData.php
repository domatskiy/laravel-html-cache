<?php
/**
 * Created by PhpStorm.
 * User: domatskiy
 * Date: 16.10.2018
 * Time: 20:03
 */

namespace Domatskiy\HtmlCache;

/**
 * Class CacheData
 * @package Domatskiy\HtmlCache
 */
class CacheData
{
    protected
        $path,
        $params;

    /**
     * @var $entity CacheData
     */
    protected static $entity;

    /**
     * @return CacheData
     */
    function getEntity()
    {
        if (!static::$entity || !(static::$entity instanceof CacheData)) {
            static::$entity = new static();
        }

        return static::$entity;
    }

    function setPath(string $path)
    {
        $this->path = $path;
    }

    function setParams(array $params)
    {
        $this->params = $params;
    }
}
