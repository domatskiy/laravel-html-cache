<?php

namespace Domatskiy\HtmlCache\Middleware;

use Closure;
use Domatskiy\HtmlCache;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

/**
 * Class HtmlCacheMiddleware
 * @package Domatskiy\HtmlCache\Middleware
 */
class HtmlCacheMiddleware
{

    protected $disk = 'html_cache';

    /**
     * @param Request $request
     * @param Closure $next
     * @param null $htmlCache
     * @return ResponseFactory|Application|Response
     * @throws FileNotFoundException
     * @throws \League\Flysystem\FileNotFoundException
     */
    public function handle(Request $request, Closure $next, $htmlCache = null)
    {
        $enable = true;
        $arParams = [];

        $user_id = \Auth::id();

        /**
         * проверка параметров
         */
        $for_authorized = config('html-cache.for_authorized', true);
        if ($htmlCache == 'disable' || ($user_id && !$for_authorized) || $request->ajax()) {
            $enable = false;
        } else if($htmlCache) {
            $ar = explode('|', $htmlCache);
            $arParams = $request->all($ar);

            foreach ($arParams as $index => $val) {
                if ($val === null || $val === '') {
                    unset($arParams[$index]);
                }
            }
        }

        if ($enable) {
            $HTMLCache = new HtmlCache\HtmlStorage($request->path(), $arParams);
            $Content = $HTMLCache->getContent();

            if ($Content->getContent() && $Content->getDate()) {
                // If-Modified-Since
                $ifModifiedSince = 'If-Modified-Since';
                if ($request->hasHeader('if-modified-since')) {
                    $ifModifiedSince = 'if-modified-since';
                }

                $delta = $request->hasHeader($ifModifiedSince) ? strtotime($request->header($ifModifiedSince)) - $Content->getDate()->getTimestamp() : null;
                // Log::debug('html last '.$ifModifiedSince.': '.var_export($request->header($ifModifiedSince), true).', time: '.strtotime($request->header($ifModifiedSince)).' / '.$Content->getDate()->format('Y-m-d H:i:s').', delata: '.$delta);

                if ($delta !== null && ($delta < 60 || $delta > -60)) {
                    // cache IS current, respond 304
                    return response('', 304)
                        ->setLastModified($Content->getDate());
                }

                $header = [
                    'Content-Type' => 'text/html; charset=utf-8'
                ];

                return response($Content->getContent(), 200, $header)
                    ->setLastModified($Content->getDate());
            }
        }

        /**
         * @var $response Response
         */
        $response = $next($request);

        if($enable && $response->status() === 200 && $request->isMethod('GET')) {
            $buffer = $response->getContent();

            if (isset($HTMLCache)) {
                $HTMLCache->saveContent($buffer);
            }
        }

        return $response;
    }
}
