<?php

namespace Domatskiy\HtmlCache\HtmlStorage;

use DateTime;

/**
 * Class Content
 * @package Domatskiy\HtmlCache\HtmlStorage
 */
class Content
{
    /**
     * @var null|string
     */
    protected $content;

    /**
     * @var null|DateTime
     */
    protected $date;

    /**
     * Content constructor.
     * @param string|null $content
     * @param DateTime|null $date
     */
    public function __construct(?string $content = null, ?DateTime $date = null)
    {
        $this->content = $content;
        $this->date = $date;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function getDate(): ?DateTime
    {
        return $this->date;
    }
}
