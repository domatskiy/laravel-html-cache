<?php

namespace Domatskiy\HtmlCache\HtmlStorage\HtmlPath;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Key
 * @package Domatskiy\HtmlCache\HtmlStorage\HtmlPath
 */
class Key extends Model
{
    /**
     * Таблица БД, используемая моделью.
     * @var string
     */
    protected $table = 'html_cache_key';

    /**
     * Атрибуты, исключенные из JSON-представления модели.
     *
     * @var array
     */
    protected $hidden = array();

    protected $fillable = array(
        'cache_id',
        'key',
    );
}
