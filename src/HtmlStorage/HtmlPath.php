<?php

namespace Domatskiy\HtmlCache\HtmlStorage;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HtmlPath
 * @package Domatskiy\HtmlCache\HtmlStorage
 */
class HtmlPath extends Model
{
    /**
     * Таблица БД, используемая моделью.
     * @var string
     */
    protected $table = 'html_cache';

    /**
     * Атрибуты, исключенные из JSON-представления модели.
     *
     * @var array
     */
    protected $hidden = array();

    protected $fillable = array('path');
}
