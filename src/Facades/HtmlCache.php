<?php

namespace Domatskiy\HtmlCache\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class HtmlCache
 * @package Domatskiy\HtmlCache\Facades
 */
class HtmlCache extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Domatskiy\HtmlCache';
    }
}
