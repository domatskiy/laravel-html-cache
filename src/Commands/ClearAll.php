<?php

namespace Domatskiy\HtmlCache\Commands;

use Domatskiy\HtmlCache\HtmlStorage;
use Illuminate\Console\Command;

use Illuminate\Console\OutputStyle;
use Symfony\Component\Console\Input;
use Symfony\Component\Console\Output\ConsoleOutput;

/**
 * Class ClearAll
 * @package Domatskiy\HtmlCache\Commands
 */
class ClearAll extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'html-cache:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear html cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @throws \Exception
     */
    public function handle()
    {
        $output = new OutputStyle(new Input\ArgvInput(), new ConsoleOutput());
        $output->title($this->description);

        HtmlStorage::removeAllHtmlFiles();
        HtmlStorage::truncateBase();

        $output->success('html cache cleared');
    }
}
