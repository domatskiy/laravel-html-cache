<?php

namespace Domatskiy\HtmlCache;

/**
 * Class CacheKeysRegistrator
 * @package Domatskiy\HtmlCache
 */
class CacheKeysRegistrator
{
    protected $keys = [];

    /**
     * @param string $key
     */
    function addKey(string $key): void
    {
        $this->keys[] = $key;
    }

    /**
     * @return array
     */
    function getKeys(): array
    {
        return $this->keys;
    }

    function deleteKey(string  $key):void
    {
        // TODO
    }
}
