<?php

namespace Domatskiy\HtmlCache;

use Domatskiy\HtmlCache\Commands\ClearAll;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;

class HtmlCacheServiceProvider extends ServiceProvider
{
    /**
     * Bootstrapping the package.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/html-cache.php' => config_path('html-cache.php'),
        ], 'htmlcache');

        if ($this->app->runningInConsole()) {
            $this->commands([
                ClearAll::class
            ]);
        }

        $this->loadMigrationsFrom(__DIR__.'/migrations');

        Event::subscribe(\Domatskiy\HtmlCache\Listeners\CacheEventSubscriber::class);

        #Event::listen('cache:clearing', function () { });

        Event::listen('cache:cleared', function () {

            /**
             * clear base of html cache keys
             */

            // удаление файлов html кэша
            HtmlStorage::removeAllHtmlFiles();

            // очистка базы html кэша
            HtmlStorage::truncateBase();
        });

        $this->app->singleton('CacheKeysRegistrator', function ($app) {
            return new \Domatskiy\HtmlCache\CacheKeysRegistrator();
        });
    }

    /**
     * Bind some Interfaces and implementations.
     */
    protected function bootBindings()
    {
        /*$this->app->singleton('HtmlCacheStorage', function ($app) {
            return new \Domatskiy\HtmlCache\HtmlStorage();
        });*/

    }
}
