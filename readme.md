# Laravel HTML Cache

###Install

```bash
composer require domatskiy/laravel-html-cache
```

Publish needed
```bash
php artisan vendor:publish --provider="Domatskiy\HtmlCache\HtmlCacheServiceProvider"
```

Add information about the provider to the config/app.php => providers

```php
Domatskiy\HtmlCache\HtmlCacheServiceProvider::class
``` 

to the config/app.php => aliases

```php
'HtmlCache' => Domatskiy\HtmlCache\Facades\HtmlCache::class,
``` 

add Middleware to the App\Http\Kernel.php => $routeMiddleware
```php
'html-cache' => \Domatskiy\HtmlCache\Middleware\HTMLCacheMiddleware::class
```

###Use in routers

```php
Route::middleware(['html-cache'])->group(function (){
    # your routes 
});
```

###Commands
clearing the html cache
```bash
 php artisan html-cache:clear
```

#Usage
Register the middleware in the Kernel file:
app\Http\Kernel.php
```php
protected $routeMiddleware = [
    ...
    'html-cache' => \Domatskiy\HtmlCache\Middleware\HtmlCacheMiddleware::class,
    ...
];
```

Use middleware in the routes 

```php
Route::group(['middleware' => ['html-cache']], function () { 
    Route::get('/', [
         'uses' => 'IndexController@index'
     ]);
});

# To return different pages from the html cache when changing query parameters
# Example: tracking parameters page and section_id
Route::group(['middleware' => ['html-cache:page,section_id']], function () { 
    Route::get('/lessons', [
         'uses' => 'IndexController@index'
     ]);
});
```
